import React, { Component } from 'react'
import { Item, Input } from 'native-base'

export default class TextInput extends Component {
  render() {
    const {
      name,
      placeholder,
      form
    } = this.props;

    return (
      <Item>
        <Input placeholder={placeholder || name} onChangeText={(text) => form.setState({[name]: text})}/>
      </Item>
    )
  }
}
