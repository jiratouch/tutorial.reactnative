import React, { Component } from 'react'
import { Text, Toast } from 'native-base';
import axios from 'axios';

export default class Create extends Component {
  apicaller = axios.post
  submit = async (data) => {
    const {
      path,
      afterError,
      afterSave,
    } = this.props;
    try {
      await this.apicaller(path, data);
      Toast.show({
        text: 'Data save successfully',
        type: 'success'
      })
      afterSave && afterSave()
    } catch (e) {
      console.log(e)
      Toast.show({
        text: 'Error occur',
        type: 'danger'
      })
      afterError && afterError()
    }
  }
  render() {
    const children = !this.props.children.length ? [this.props.children] : this.props.children
    return (
      <React.Fragment>
        {
          children.map((elm, index) => React.cloneElement(elm, {
            key: index,
            onSubmit: this.submit
          }))
        }
      </React.Fragment>
    )
  }
}
