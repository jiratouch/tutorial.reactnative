import React, { Component } from 'react'
import {
  Item,
  Button,
  Text
} from 'native-base'

export default class SubmitButton extends Component {
  render() {
    const {
      form,
      text
    } = this.props
    return (
      <Item>
        <Button onPress={() => form.submit()}>
          <Text>{text}</Text>
        </Button>
      </Item>
    )
  }
}
