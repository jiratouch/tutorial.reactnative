
import React, { Component } from 'react'
import Create from './Create'
import axios from 'axios'

export default class Edit extends Create, Show {
  apicaller = axios.patch
}
