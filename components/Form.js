import React, { Component } from 'react'
import { Form } from 'native-base'

export default class MyForm extends Component {
  submit = () => {
    const {
      onSubmit
    } = this.props;
    onSubmit(this.state);
  }
  render() {
    const children = !this.props.children.length ? [this.props.children] : this.props.children
    return (
      <Form>
        {
          children.map((elm, index) => React.cloneElement(elm, {
            form: this,
            key: index
          }))
        }
      </Form>
    )
  }
}
