import { createStackNavigator, createAppContainer } from 'react-navigation';
import SigninScreen from '../screens/SigninScreen';
import SignupScreen from '../screens/SignupScreen';
const AppNavigator = createStackNavigator({
    SignIn: SigninScreen,
    SignUp: SignupScreen
}, {
    initialRouteName: 'SignUp'
})

export default createAppContainer(AppNavigator);