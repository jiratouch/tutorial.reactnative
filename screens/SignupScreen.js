import React, { Component } from 'react'
import { Container, Text, Button, Content } from 'native-base'
import Create from '../components/Create'
import Form from '../components/Form'
import TextInput from '../components/TextInput'
import SubmitButton from '../components/SubmitButton'

export default class SignupScreen extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Text>Signup screen</Text>
          <Button onPress={() => this.props.navigation.navigate('SignIn')}>
            <Text>Sign in</Text>
          </Button>
          <Create path="http://localhost:3000/api/users" afterSave={() => this.props.navigation.navigate('SignIn')}>
            <Form>
              <TextInput name="email" />
              <TextInput name="username" />
              <TextInput name="password" />
              <SubmitButton text="Sign up"/>
            </Form>
          </Create>
        </Content>
      </Container>
    )
  }
}
