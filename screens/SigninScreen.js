import React, { Component } from 'react'
import { Container, Text, Button, Content, Item, Input, Toast } from 'native-base'
import Create from '../components/Create'
import Form from '../components/Form'
import TextInput from '../components/TextInput'
import SubmitButton from '../components/SubmitButton'

export default class SigninScreen extends Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <Container>
        <Content>
          <Text>Signup screen</Text>
          <Button onPress={() => this.props.navigation.navigate('Signup')}>
            <Text>Sign in</Text>
          </Button>
          <Create path="http://localhost:3000/api/users/login">
            <Form>
              <TextInput name="username" />
              <TextInput name="password" />
              <SubmitButton text="Sign in"/>
            </Form>
          </Create>
        </Content>
      </Container>
    )
  }
}

{/* <Create path="/api/users" afterSave={() => } afterError={() =>} >
  <Form>
    <TextInput name="username"/>
    <TextInput name="password" type="password"/>
    <SubmitButton text="Signin"/>
  </Form>
</Create> */}